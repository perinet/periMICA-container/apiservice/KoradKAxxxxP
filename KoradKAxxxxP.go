/*
 * Copyright (c) Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package apiservice_KoradKAxxxxP

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	server "gitlab.com/perinet/generic/lib/httpserver"
	rbac "gitlab.com/perinet/generic/lib/httpserver/rbac"
	filestorage "gitlab.com/perinet/generic/lib/utils/filestorage"
	webhelper "gitlab.com/perinet/generic/lib/utils/webhelper"
	serial "go.bug.st/serial"
	"go.bug.st/serial/enumerator"
)
 
 type PathInfo = server.PathInfo
 
 type KoradKAxxxxP_info struct {
	 ApiVersion string                `json:"api_version"`
	 Config     KoradKAxxxxP_config   `json:"config"`
 }
 
 
 type KoradKAxxxxP_config struct {
	 Vid string `json:"vid"`
	 Pid string `json:"pid"`
	 Baudrate  int  `json:"baudrate"`
	 Portname  string `json:"portname"`
 }

 type KoradKAxxxxP_voltage struct {
	Voltage float64 `json:"voltage"`
}

type KoradKAxxxxP_current struct {
	Current float64 `json:"current"`
}

type KoradKAxxxxP_output_status struct {
	Output_status bool `json:"outputStatus"`
}
 
 const (
	 API_VERSION = "1"
	 CONFIG_FILE = "/var/lib/apiservice/KoradKAxxxxP/config.json"
	 read_timeout = 2 * time.Second
 )
 
 var (
	 Logger log.Logger = *log.Default()
 
	 // Default config
	 config_cache = KoradKAxxxxP_config{
		 Vid: "0416",
		 Pid: "5011",
		 Baudrate: 115200,
		 Portname: "",
	 }

	serial_port serial.Port
 )

 func findPort() (string, error) {
    
	ports, err := enumerator.GetDetailedPortsList()
	Logger.Println("ports: ", ports)
    
	if err != nil {
        return "", err
    }

	VID_PID := config_cache.Vid + ":" + config_cache.Pid

    for _, port := range ports {

		Logger.Println("found port.Name=", port.Name, "Vid: ", port.VID, "Pid: ", port.PID)
		
		if config_cache.Vid == port.VID && config_cache.Pid == port.PID {
			return port.Name, nil
		}
    }

    return "", fmt.Errorf("Could not find any port matching Vendor/Product ID combination %s", VID_PID)
}
 
 func init() {
	 Logger.SetPrefix("ApiServiceKoradKAxxxxP: ")
	 Logger.Println("Starting")
 
	 // load config
	 var tmp KoradKAxxxxP_config

	 err := filestorage.LoadObject(CONFIG_FILE, &tmp)
	 if err != nil {
		 Logger.Print(err)
	 } else {
		 config_cache = tmp
	 }
 
	 config_cache.Portname, err = findPort()

	 if err != nil {
		Logger.Print(err)
    }

	Logger.Print("KoradKAxxxxP Found port:", config_cache.Portname)
 }
 
 // URL endpoint definition of the service
 func PathsGet() []PathInfo {
	 return []PathInfo{
		 {Url: "/KoradKAxxxxP", Method: server.GET, Role: rbac.NONE, Call: KoradKAxxxxP_get},
		 {Url: "/KoradKAxxxxP/config", Method: server.GET, Role: rbac.NONE, Call: config_get},
		 {Url: "/KoradKAxxxxP/config", Method: server.PATCH, Role: rbac.NONE, Call: config_set},
		 {Url: "/KoradKAxxxxP/device-identification", Method: server.GET, Role: rbac.NONE, Call: device_identification_get},
		 {Url: "/KoradKAxxxxP/command", Method: server.PUT, Role: rbac.SUPERUSER, Call: command_set},
		 {Url: "/KoradKAxxxxP/voltage", Method: server.GET, Role: rbac.SUPERUSER, Call: voltage_get},
		 {Url: "/KoradKAxxxxP/voltage", Method: server.PATCH, Role: rbac.SUPERUSER, Call: voltage_set},
		 {Url: "/KoradKAxxxxP/current", Method: server.GET, Role: rbac.SUPERUSER, Call: current_get},
		 {Url: "/KoradKAxxxxP/current", Method: server.PATCH, Role: rbac.SUPERUSER, Call: current_set},
		 {Url: "/KoradKAxxxxP/power/enable", Method: server.PATCH, Role: rbac.SUPERUSER, Call: power_enable_set},
		 {Url: "/KoradKAxxxxP/power/disable", Method: server.PATCH, Role: rbac.SUPERUSER, Call: power_disable_set},
	 }
 }
 
 func KoradKAxxxxP_get(w http.ResponseWriter, r *http.Request) {
	 var http_status int = http.StatusOK
	 var info = KoradKAxxxxP_info{
		 ApiVersion: API_VERSION,
		 Config:     config_cache,
	 }
	 res, err := json.Marshal(info)
	 if err != nil {
		 http_status = http.StatusInternalServerError
		 res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	 }
	 webhelper.JsonResponse(w, http_status, res)
 }
 
 func config_get(w http.ResponseWriter, r *http.Request) {
	 var http_status int = http.StatusOK
	 data, err := json.Marshal(config_cache)
	 if err != nil {
		 http_status = http.StatusInternalServerError
		 data = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	 }
	 webhelper.JsonResponse(w, http_status, data)
 }
 
 func config_set(w http.ResponseWriter, r *http.Request) {
	 payload, _ := io.ReadAll(r.Body)
	 var tmp_cfg KoradKAxxxxP_config
	 err := json.Unmarshal(payload, &tmp_cfg)
	 if err != nil {
		 data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		 webhelper.JsonResponse(w, http.StatusBadRequest, data)
		 return
	 }
 
	 err = filestorage.StoreObject(CONFIG_FILE, tmp_cfg)
	 if err != nil {
		 data := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		 webhelper.JsonResponse(w, http.StatusInternalServerError, data)
		 return
	 }
 
	 webhelper.EmptyResponse(w, http.StatusNoContent)
 }
 
 func command_set(w http.ResponseWriter, r *http.Request) {
	var http_status int = http.StatusOK

	payload, _ := io.ReadAll(r.Body)

	res, err := send_command(string(payload))
	if err != nil {
		http_status = http.StatusInternalServerError
		res := json.RawMessage(`{"command_set error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http_status, res)
	}
	
	webhelper.JsonResponse(w, http_status, res)
}

func device_identification_get(w http.ResponseWriter, r *http.Request) {
	
	var http_status int = http.StatusOK
	
	// send due command
	res, err := send_command("*IDN?")
	if err != nil {
		http_status = http.StatusInternalServerError
		res := json.RawMessage(`{"device_identification error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http_status, res)
	}

	response := json.RawMessage(`{"identification": "`+ string(res) + `"}`)

	webhelper.JsonResponse(w, http_status, response)
}

func voltage_get(w http.ResponseWriter, r *http.Request) {

	var http_status int = http.StatusOK

	res, err := send_command("VOUT1?")

	if err != nil {
		http_status = http.StatusInternalServerError
		res := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http_status, res)
	}
	response := json.RawMessage(`{"voltage": `+ string(res) + `}`)

	webhelper.JsonResponse(w, http_status, response)
}

func voltage_set(w http.ResponseWriter, r *http.Request) {
	
	payload, _ := io.ReadAll(r.Body)

	var voltage_config KoradKAxxxxP_voltage

	err := json.Unmarshal(payload, &voltage_config)
	if err != nil {
		webhelper.EmptyResponse(w, http.StatusBadRequest)
		return
	}

	res, err := send_command("VSET1:" + strconv.FormatFloat(voltage_config.Voltage, 'f', -1, 64))
	if err != nil {
		http_status := http.StatusInternalServerError
		res := json.RawMessage(`{"command_set error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http_status, res)
	}
	
	webhelper.JsonResponse(w, http.StatusNoContent, res)
}

func current_get(w http.ResponseWriter, r *http.Request) {

	var http_status int = http.StatusOK

	res, err := send_command("ISET1?")

	if err != nil {
		http_status = http.StatusInternalServerError
		res := json.RawMessage(`{"error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http_status, res)
	}
	response := json.RawMessage(`{"current": `+ string(res) + `}`)
	webhelper.JsonResponse(w, http_status, response)
}

func current_set(w http.ResponseWriter, r *http.Request) {
	
	payload, _ := io.ReadAll(r.Body)

	var current_config KoradKAxxxxP_current

	err := json.Unmarshal(payload, &current_config)
	if err != nil {
		webhelper.EmptyResponse(w, http.StatusBadRequest)
		return
	}

	res, err := send_command("ISET1:" + strconv.FormatFloat(current_config.Current, 'f', -1, 64))
	if err != nil {
		http_status := http.StatusInternalServerError
		res := json.RawMessage(`{"command_set error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http_status, res)
	}
	
	webhelper.JsonResponse(w, http.StatusNoContent, res)
}

func power_enable_set(w http.ResponseWriter, r *http.Request) {

	res, err := send_command("OUT1")
	if err != nil {
		http_status := http.StatusInternalServerError
		res := json.RawMessage(`{"command_set error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http_status, res)
	}
	
	webhelper.JsonResponse(w, http.StatusNoContent, res)
}

func power_disable_set(w http.ResponseWriter, r *http.Request) {

	res, err := send_command("OUT0")
	if err != nil {
		http_status := http.StatusInternalServerError
		res := json.RawMessage(`{"command_set error": "` + err.Error() + `"}`)
		webhelper.JsonResponse(w, http_status, res)
	}
	
	webhelper.JsonResponse(w, http.StatusNoContent, res)
}

func send_command(command string) (string, error) {
	
	// configure the mode
	mode := &serial.Mode{
		BaudRate: config_cache.Baudrate,
		StopBits: serial.OneStopBit,
	}

	// open serial port
	serial_port, err := serial.Open(config_cache.Portname, mode)
	if err != nil {
		Logger.Println("send_command Open: ", err)
		return "", err
	}

	// command to send
	bytes_to_send := []byte(command)

	// send the command to read the identification
	_, err = serial_port.Write(bytes_to_send)
	if err != nil {
		Logger.Println("send_command Write: ", err)
		return "", err
	}
	
	buffer := make([]byte, 128)
	
	_, err = serial_port.Read(buffer)
	if err != nil {
		return "Error reading from port:", err
	}
		
	result := bytes.TrimRight(buffer, "\x00")
	return string(result), err
		
}