module gitlab.com/perinet/periMICA-container/apiservice/KoradKAxxxxP

go 1.20

require (
	gitlab.com/perinet/generic/lib/httpserver v0.0.0-20240304113050-0340948978be
	gitlab.com/perinet/generic/lib/utils v0.0.0-20230628130910-2f4300736d64
	go.bug.st/serial v1.6.2

)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/creack/goselect v0.1.2 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	golang.org/x/exp v0.0.0-20230510235704-dd950f8aeaea // indirect
	golang.org/x/sys v0.1.0 // indirect
)
